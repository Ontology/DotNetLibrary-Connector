﻿using DotNetLibrary_Connector.BaseConfig;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetLibrary_Connector
{
    
    [OntologyClass(IdClass = "6eb4fdd32e254886b288e1bfc2857efb")]
    public class FileItem : NotifyPropertyChange
    {
        private OntologyModDBConnector dbConnector;
        private clsLocalConfig localConfig;

        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string IdFile { get; private set; }

        [OntologyObject(PropertyType = PropertyType.Name)]
        public string NameFile { get; private set; }

        private clsOntologyItem resultLoad;
        public clsOntologyItem ResultLoad
        {
            get { return resultLoad; }
            set
            {
                resultLoad = value;
            }
        }

        public FileItem(clsLocalConfig localConfig, string fileName)
        {
            this.localConfig = localConfig;
            this.NameFile = fileName;


            Initialze();
            
        }

        private void LoadFileItem()
        {
            var searchFile = new List<clsOntologyItem> { new clsOntologyItem
                {
                    Name = NameFile,
                    GUID_Parent = localConfig.OItem_class_file.GUID
                }
            };

            var result = dbConnector.GetDataObjects(searchFile);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchRelations = dbConnector.Objects1.Select(fileItem => new clsObjectRel
                {
                    ID_Object = fileItem.GUID,
                    ID_Parent_Other = localConfig.OItem_class_folder.GUID
                }).ToList();

                if (searchRelations.Any())
                {
                    var dbConnectorRel = new OntologyModDBConnector(localConfig.Globals);

                    result = dbConnectorRel.GetDataObjectRel(searchRelations, doIds: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var fileItem = (from fileItem1 in dbConnector.Objects1
                                        join relItem in dbConnectorRel.ObjectRelsId on fileItem1.GUID equals relItem.ID_Object into relItems
                                        from relItem in relItems.DefaultIfEmpty()
                                        where relItem == null
                                        select fileItem1).FirstOrDefault();

                        if (fileItem != null)
                        {
                            IdFile = fileItem.GUID;
                            NameFile = fileItem.Name;
                        }
                    }
                }
                
                if (string.IsNullOrEmpty(IdFile))
                {
                    IdFile = localConfig.Globals.NewGUID;
                }


                
            }

            resultLoad = dbConnector.SaveModel(new List<object> { this });

            ResultLoad = result;
        }

        private void Initialze()
        {
            dbConnector = new OntologyModDBConnector(localConfig.Globals);
            LoadFileItem();
        }
    }
}
