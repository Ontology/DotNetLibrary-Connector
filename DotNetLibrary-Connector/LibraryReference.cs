﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector.Attributes;
using DotNetLibrary_Connector.BaseConfig;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace DotNetLibrary_Connector
{
    [Flags]
    public enum ErrorType
    {
        None = 0,
        LoadError = 1,
        NotEnoughInfo = 2,
        SaveError = 4
    }
    [OntologyClass(IdClass = "649f568407ac40658ab6801e17e559ec")]
    public class LibraryReference : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbConnector;


        private ErrorType errorType;
        public ErrorType ErrorType
        {
            get { return errorType; }
            set
            {
                errorType = value;
                RaisePropertyChanged(NotifyChanges.LibraryReference_ErrorType);
            }
        }

        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string IdReference { get; set; }
        
        [OntologyObject(PropertyType = PropertyType.Name)]
        public string NameReference
        {
            get
            {
                var lengthSoftwareDevelopment = 0;
                var lengthLibrary = 0;
                var name = "";

                if (SoftwareDevelopment != null)
                {
                    name = SoftwareDevelopment.NameSoftwareDevelopment;
                    lengthSoftwareDevelopment = SoftwareDevelopment.NameSoftwareDevelopment.Length;
                    if (lengthSoftwareDevelopment > 127)
                    {
                        name = name.Substring(0, 126);
                    }
                }
                
                if (Library != null)
                {
                    lengthLibrary = Library.NameFile.Length;

                    var nameLibrary = Library.NameFile;
                    if (lengthLibrary > 127)
                    {
                        name += "_" + nameLibrary.Substring(0, 126);
                    }
                    else
                    {
                        name += "_" + nameLibrary;
                    }
                }

                return name;
            }
        }

        

        private SoftwareDevelopment softwareDevelopment;
        [ObjectRelation(IdParentOther = "71415eebce464b2cb0a2f72116b55438", IdRelationType = "e07469d9766c443e85266d9c684f944f", Replace = true, RelationSearchType = RelationSearchType.OntologyItem)]
        public SoftwareDevelopment SoftwareDevelopment
        {
            get
            {
                return softwareDevelopment;
            }
            set
            {
                softwareDevelopment = value;
                GetLibraryReference();
            }
        }

        private FileItem library;
        [ObjectRelation(IdParentOther = "6eb4fdd32e254886b288e1bfc2857efb", IdRelationType = "dc1e641644374c2fa9074225f94ee06f", Replace = true, RelationSearchType = RelationSearchType.OntologyItem)]
        public FileItem Library
        {
            get
            {
                return library;
            }
            set
            {
                library = value;
                GetLibraryReference();
            }
        }

        [ObjectRelation(IdParentOther = "f47a3e1c50b44666a41de975597c9adb", IdRelationType = "1b7e92c664d7417bb3dcd705e8605620", Replace = true, RelationSearchType = RelationSearchType.OntologyItem)]
        public FolderItem PackageFolder { get; set; }

        [ObjectRelation(IdParentOther = "f47a3e1c50b44666a41de975597c9adb", IdRelationType = "2441081151f5453999a8aee2797c2edf", Replace = true, RelationSearchType = RelationSearchType.OntologyItem)]
        public FolderItem FrameworkFolder { get; set; }

        [ObjectRelation(IdParentOther = "f47a3e1c50b44666a41de975597c9adb", IdRelationType = "6e61ff1d295c4d3f8612461d82f23214", Replace = true, RelationSearchType = RelationSearchType.OntologyItem)]
        public FolderItem VersionFolder { get; set; }

        public ErrorType SaveLibraryReference()
        {
            ErrorType = ErrorType.None;

            if (SoftwareDevelopment == null
                || Library == null
                || PackageFolder == null
                || FrameworkFolder == null
                || VersionFolder == null)
            {
                ErrorType = ErrorType.NotEnoughInfo;
                return ErrorType;
            }

            var result = dbConnector.SaveModel(new List<object> { this });

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ErrorType = ErrorType.SaveError;
            }

            return ErrorType;
        }

        private void GetLibraryReference()
        {
            if (SoftwareDevelopment == null || Library == null) return;

            var searchLibrary = new List<clsOntologyItem> { new clsOntologyItem
                {
                    Name = NameReference,
                    GUID_Parent = localConfig.OItem_class_dot_net_reference_configuration.GUID
                }
            };

            var result = dbConnector.GetDataObjects(searchLibrary);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                ErrorType |= ErrorType.LoadError;
                return;
            }

            var library = dbConnector.Objects1.FirstOrDefault(libraryConfig => libraryConfig.Name.ToLower() == NameReference.ToLower());

            if (library == null)
            {
                IdReference = localConfig.Globals.NewGUID;
            }
            else
            {
                IdReference = library.GUID;
            }

        }

        public LibraryReference(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;

            Initialize();
        }

        private void Initialize()
        {
            dbConnector = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
