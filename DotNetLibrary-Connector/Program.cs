﻿using CommandLineParser;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DotNetLibrary_Connector
{
    class Program
    {
        private static clsLocalConfig localConfig;
        private static ArgumentParser argumentParser = new ArgumentParser();
        static void Main(string[] args)
        {
            Initialize();

            argumentParser.AddArguments(args, "=");

            if (argumentParser.ArgumentItems.Any(arg => !arg.IsArgValid()))
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            var development = argumentParser.ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == "Dev".ToLower());
            var library = argumentParser.ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == "Lib".ToLower());
            var packageFolder = argumentParser.ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == "PackageFolder".ToLower());
            var frameworkFolder = argumentParser.ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == "FrameworkFolder".ToLower());
            var versionFolder = argumentParser.ArgumentItems.FirstOrDefault(arg => arg.Name.ToLower() == "VersionFolder".ToLower());

            if (development == null || string.IsNullOrEmpty(development.Value) || !development.IsArgValid())
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            if (library == null || string.IsNullOrEmpty(library.Value) || !library.IsArgValid())
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            if (packageFolder == null || string.IsNullOrEmpty(packageFolder.Value) || !packageFolder.IsArgValid())
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            if (frameworkFolder == null || string.IsNullOrEmpty(frameworkFolder.Value) || !frameworkFolder.IsArgValid())
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            if (versionFolder == null || string.IsNullOrEmpty(versionFolder.Value) || !versionFolder.IsArgValid())
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            var developmentItem = new SoftwareDevelopment(localConfig, development.Value);

            if (developmentItem.ResultLoad.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OutSyntax();
                Environment.Exit(-1);
            }

            var libraryItem = new FileItem(localConfig, library.Value);

            if (libraryItem.ResultLoad.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OutSyntax("Error while creating the library item");
                Environment.Exit(-1);
            }

            var packageFolderItem = new FolderItem(localConfig, packageFolder.Value);

            if (packageFolderItem.ResultLoad.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OutSyntax("Error while creating the packagefolder item");
                Environment.Exit(-1);
            }

            var frameworkFolderItem = new FolderItem(localConfig, frameworkFolder.Value);

            if (frameworkFolderItem.ResultLoad.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OutSyntax("Error while creating the fromaeworkfolder item");
                Environment.Exit(-1);
            }

            var versionFolderItem = new FolderItem(localConfig, versionFolder.Value);

            if (versionFolderItem.ResultLoad.GUID == localConfig.Globals.LState_Error.GUID)
            {
                OutSyntax("Error while creating the versionfolder item");
                Environment.Exit(-1);
            }

            var libraryReference = new LibraryReference(localConfig);

            libraryReference.SoftwareDevelopment = developmentItem;
            libraryReference.Library = libraryItem;
            libraryReference.PackageFolder = packageFolderItem;
            libraryReference.FrameworkFolder = frameworkFolderItem;
            libraryReference.VersionFolder = versionFolderItem;

            libraryReference.SaveLibraryReference();

            if (libraryReference.ErrorType.HasFlag(ErrorType.LoadError))
            {
                OutSyntax("Error while getting LibraryReference");
                Environment.Exit(-1);
            }

        }

        private static void Initialize()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            ConfigureArguments();
        }

        
        private static void ConfigureArguments()
        {
            argumentParser.AddArgument("Dev", ArgumentType.String);
            argumentParser.AddArgument("Lib", ArgumentType.String);
            argumentParser.AddArgument("PackageFolder", ArgumentType.String);
            argumentParser.AddArgument("FrameworkFolder", ArgumentType.String);
            argumentParser.AddArgument("VersionFolder", ArgumentType.String);


        }

        private static void OutSyntax(string error = null)
        {
            if (!string.IsNullOrEmpty(error))
            {
                Console.WriteLine(error);
            }

            Console.WriteLine(Properties.Settings.Default.SyntaxHelp);    
        }
    }
}
