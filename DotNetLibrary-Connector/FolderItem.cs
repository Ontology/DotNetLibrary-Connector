﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DotNetLibrary_Connector
{
    [OntologyClass(IdClass = "f47a3e1c50b44666a41de975597c9adb")]
    public class FolderItem
    {
        private OntologyModDBConnector dbConnector;
        private clsLocalConfig localConfig;

        private clsOntologyItem resultLoad;
        public clsOntologyItem ResultLoad
        {
            get { return resultLoad; }
            set
            {
                resultLoad = value;
            }
        }

        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string IdFolder { get; set; }

        [OntologyObject(PropertyType = PropertyType.Name)]
        public string NameFolder { get; set; }

        public FolderItem(clsLocalConfig localConfig, string folderName)
        {
            this.localConfig = localConfig;
            this.NameFolder = folderName;

            Initialze();
        }

        private void LoadFolderItem()
        {
            var regexVersion = @"\.\d\.\d\.\d\.\d";
            var regexItem = new Regex(regexVersion);

            var nameFolder = regexItem.Replace(NameFolder, "");

            var searchFolder = new List<clsOntologyItem> { new clsOntologyItem
                {
                    Name = nameFolder,
                    GUID_Parent = localConfig.OItem_class_folder.GUID
                }
            };

            var result = dbConnector.GetDataObjects(searchFolder);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchRelations = dbConnector.Objects1.Where(folderItem => folderItem.Name.ToLower() == NameFolder.ToLower()).Select(folderItem => new clsObjectRel
                {
                    ID_Object = folderItem.GUID,
                    ID_Parent_Other = localConfig.OItem_class_folder.GUID
                }).ToList();

                if (searchRelations.Any())
                {
                    var dbConnectorRel = new OntologyModDBConnector(localConfig.Globals);

                    result = dbConnectorRel.GetDataObjectRel(searchRelations, doIds: true);

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        var folderItem = (from folderItem1 in dbConnector.Objects1.Where(folderItem2 => folderItem2.Name.ToLower() == NameFolder.ToLower())
                                          join relItem in dbConnectorRel.ObjectRelsId on folderItem1.GUID equals relItem.ID_Object into relItems
                                        from relItem in relItems.DefaultIfEmpty()
                                        where relItem == null
                                        select folderItem1).FirstOrDefault();

                        if (folderItem != null)
                        {
                            IdFolder = folderItem.GUID;
                            NameFolder = folderItem.Name;
                        }
                    }
                }

                if (string.IsNullOrEmpty(IdFolder))
                {
                    IdFolder = localConfig.Globals.NewGUID;
                }



            }

            resultLoad = dbConnector.SaveModel(new List<object> { this });

            ResultLoad = result;
        }

        private void Initialze()
        {
            dbConnector = new OntologyModDBConnector(localConfig.Globals);
            LoadFolderItem();
        }

    }
}
