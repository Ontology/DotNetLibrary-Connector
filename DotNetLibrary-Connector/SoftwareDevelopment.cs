﻿using DotNetLibrary_Connector.BaseConfig;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Attributes;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetLibrary_Connector
{
    [OntologyClass(IdClass = "71415eebce464b2cb0a2f72116b55438")]
    public class SoftwareDevelopment : NotifyPropertyChange
    {
        private OntologyModDBConnector dbConnector;
        private clsLocalConfig localConfig;

        private clsOntologyItem resultLoad;
        public clsOntologyItem ResultLoad
        {
            get { return resultLoad; }
            set
            {
                resultLoad = value;
            }
        }

        [OntologyObject(PropertyType = PropertyType.Guid)]
        public string IdSoftwareDevelopment { get; set; }

        [OntologyObject(PropertyType = PropertyType.Name)]
        public string NameSoftwareDevelopment { get; set; }

        public SoftwareDevelopment(clsLocalConfig localConfig, string sdName)
        {
            this.localConfig = localConfig;
            this.NameSoftwareDevelopment = sdName;

            Initialze();
        }

        private void Initialze()
        {
            dbConnector = new OntologyModDBConnector(localConfig.Globals);
            LoadDevelopmentItem();
        }

        private void LoadDevelopmentItem()
        {
            var searchSD = new List<clsOntologyItem> { new clsOntologyItem
                {
                    Name = NameSoftwareDevelopment,
                    GUID_Parent = localConfig.OItem_class_software_development.GUID
                }
            };

            var result = dbConnector.GetDataObjects(searchSD);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {

                var sdItem = dbConnector.Objects1.FirstOrDefault(sdItem1 => sdItem1.Name.ToLower() == NameSoftwareDevelopment.ToLower());

                if (sdItem != null)
                {
                    IdSoftwareDevelopment = sdItem.GUID;
                }
                if (string.IsNullOrEmpty(IdSoftwareDevelopment))
                {
                    IdSoftwareDevelopment = localConfig.Globals.NewGUID;
                }



            }
            resultLoad = dbConnector.SaveModel(new List<object> { this });

            ResultLoad = result;
        }
    }
}
